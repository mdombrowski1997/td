/********************************************************************************************
// None of the facts have changed, only his perspective --Ideas At Play: Atlanta and Louie //
********************************************************************************************/
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

//globals
const int MAP_WIDTH = 8192;
const int MAP_HEIGHT = 6144;
const int WIN_WIDTH = 1024;
const int WIN_HEIGHT = 768;
const int CAMBUF = 128;
const double PLAYER_SPEED = 10;
const int WALL_COUNT = 4;
const int NPC_COUNT = 10;
const int TERM_COUNT = 10;
const int BUDS_COUNT = 16;
const int COLLISION_OVERLAP = 16;
const char* FIRST_LAYOUT = "layouts/BASE.lt";
const double SCALE = WIN_WIDTH/(4*MAP_WIDTH);
//enum for input handling
enum InputMode { INPUT_MODE_PLAY, INPUT_MODE_PHONE };
//enum for keypress
enum key { W, A, S, D };
//type enum for Sprites
enum Type { SPRITE_TYPE_NONE = 0, SPRITE_TYPE_PLAYER, SPRITE_TYPE_BG, SPRITE_TYPE_NPC, SPRITE_TYPE_TERM, SPRITE_TYPE_PHONE, SPRITE_TYPE_WALL, SPRITE_TYPE_LIGHT, SPRITE_TYPE_BUDS, SPRITE_TYPE_DIM, SPRITE_TYPE_BORDER };
enum Subtype { SPRITE_SUBTYPE_NONE = 0, SPRITE_SUBTYPE_WALL_TOP, SPRITE_SUBTYPE_WALL_RIGHT, SPRITE_SUBTYPE_WALL_BOTTOM, SPRITE_SUBTYPE_WALL_LEFT };

//two dimensional data holder
struct Vec
{
    double x = 0;
    double y = 0;
};

//Sprite class for screen objects
class Sprite
{
public:
    //Sprite(SDL_Renderer* renderer, Type type, SDL_Rect rect);
    Sprite();
    ~Sprite();

    /**
    // Essentially doing the constructors job, but I can't call the constructor
    // dynamicallly, so I call this after default constructor instead
    // 
    // renderer - SDL_Renderer* for window, same for all objs in window
    // type - Type enum member that describes object
    // subtype - Subtype enum member that describes object
    */
    void init(SDL_Renderer* renderer, Type type, Subtype subtype = SPRITE_SUBTYPE_NONE);
    /**
    // Returns a pointer to the objects SDL_Rect
    */
    SDL_Rect* getRect();
    /**
    // Returns the type of the object
    */
    Type getType();
    /**
    // Returns the subtype of the object
    */
    Subtype getSubtype();
    /**
    // Used to set the position of this object.
    // Useful for nudging caused by outside influences.
    //
    // x - new x-value for object
    // y - new y-value for object
    */
    void setPos(double x, double y);
    /**
    // Used to set the velocity of this object.
    // Useful for movement caused by outside influences.
    //
    // x - new x-value for the velocity of the object
    // y - new y-value for the velocity of the object
    */
    void setDir(double x, double y);
    Vec getDir();
    /**
    // Used to carry out frame-by-frame operations, such as movement
    */
    void update();
    /**
    // Just blits the object to the screen at its rect
    */
    void render();
    /**
    // Blits the object to the screen with its rect offset by 'cam'
    */
    void render(SDL_Rect &cam);
    void renderScale(double scale);

private:
    SDL_Renderer* renderer;
    SDL_Texture* texture;
    SDL_Rect rect;
    Vec dir;
    Type type = SPRITE_TYPE_NONE;
    Subtype subtype = SPRITE_SUBTYPE_NONE;
};

Sprite::Sprite(){}
Sprite::~Sprite(){}
void Sprite::init(SDL_Renderer* renderer, Type type, Subtype subtype)
{
    this->renderer = renderer;
    this->type = type;

    SDL_Surface* surface;
    if (type == SPRITE_TYPE_NPC)
    {
        surface = IMG_Load("art/npc.png");

        rect.x = ( rand() % MAP_WIDTH );
        rect.y = ( rand() % MAP_HEIGHT );
        rect.w = 128;
        rect.h = 256;
    }
    else if (type == SPRITE_TYPE_TERM)
    {
        surface = IMG_Load("art/term.png");

        rect.x = ( rand() % MAP_WIDTH );
        rect.y = ( rand() % MAP_HEIGHT );
        rect.w = 100;
        rect.h = 100;
    }
    else if (type == SPRITE_TYPE_PLAYER)
    {
        surface = IMG_Load("art/p1.png");

        rect.x = (MAP_WIDTH/2);
        rect.y = (MAP_HEIGHT/2);
        rect.w = 128;
        rect.h = 128;
    }
    else if (type == SPRITE_TYPE_WALL)
    {
        surface = IMG_Load("art/wall.png");

        if (subtype == SPRITE_SUBTYPE_WALL_TOP)
        {
            //top
            rect.x = 0;
            rect.y = (0-WIN_HEIGHT);
            rect.w = MAP_WIDTH;
            rect.h = WIN_HEIGHT;
        }
        else if (subtype == SPRITE_SUBTYPE_WALL_RIGHT)
        {
            //right
            rect.x = MAP_WIDTH;
            rect.y = 0;
            rect.w = WIN_WIDTH;
            rect.h = MAP_HEIGHT;
        }
        else if (subtype == SPRITE_SUBTYPE_WALL_BOTTOM)
        {
            //bottom
            rect.x = 0;
            rect.y = MAP_HEIGHT;
            rect.w = MAP_WIDTH;
            rect.h = WIN_HEIGHT;
        }
        else if (subtype == SPRITE_SUBTYPE_WALL_LEFT)
        {
            //left
            rect.x = (0-WIN_WIDTH);
            rect.y = 0;
            rect.w = WIN_WIDTH;
            rect.h = MAP_HEIGHT;
        }
    }
    else if (type == SPRITE_TYPE_PHONE)
    {
        surface = IMG_Load("art/phone.png");

        rect.x = (WIN_WIDTH/4);
        rect.y = 0;
        rect.w = (WIN_WIDTH/2);
        rect.h = WIN_HEIGHT;
    }
    else if (type == SPRITE_TYPE_BG)
    {
        surface = IMG_Load("art/grid.png");

        rect.x = 0;
        rect.y = 0;
        rect.w = MAP_WIDTH;
        rect.h = MAP_HEIGHT;
    }
    else if (type == SPRITE_TYPE_LIGHT)
    {
        surface = IMG_Load("art/light.png");

        rect.x = 0;
        rect.y = 0;
        rect.w = 256;
        rect.h = 256;
    }
    else if (type == SPRITE_TYPE_DIM)
    {
        surface = IMG_Load("art/dim.png");

        rect.x = 0;
        rect.y = 0;
        rect.w = WIN_WIDTH;
        rect.h = WIN_HEIGHT;
    }
    else if (type == SPRITE_TYPE_BUDS)
    {
        surface = IMG_Load("art/buds.png");

        rect.x = ( rand() % MAP_WIDTH );
        rect.y = ( rand() % MAP_HEIGHT );
        rect.w = 64;
        rect.h = 64;
    }
    else if (type == SPRITE_TYPE_BORDER)
    {
        surface = IMG_Load("art/border_black.png");

        rect.x = 0;
        rect.y = 0;
        rect.w = (MAP_WIDTH);
        rect.h = (MAP_HEIGHT);
    }

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
}
SDL_Rect* Sprite::getRect()
{
    return &rect;
}
Type Sprite::getType()
{
    return this->type;
}
Subtype Sprite::getSubtype()
{
    return this->subtype;
}
void Sprite::setPos(double x, double y)
{
    rect.x = x;
    rect.y = y;
}
void Sprite::setDir(double x, double y)
{
    dir.x = x;
    dir.y = y;
}
Vec Sprite::getDir()
{
    return this->dir;
}
void Sprite::update()
{
    rect.x += dir.x;
    rect.y += dir.y;
}
void Sprite::render()
{
    SDL_RenderCopy(renderer, texture, NULL, &rect);
}
void Sprite::render(SDL_Rect &cam)
{
    //offset rendering with camera rect
    SDL_Rect render{ (rect.x - cam.x), (rect.y - cam.y), rect.w, rect.h };

    //only render what's visible through(colliding with) camera
    if (rect.x < cam.x + cam.w &&
        cam.x < rect.x + rect.w &&
        rect.y < cam.y + cam.h &&
        cam.y < rect.y + rect.h) 
        SDL_RenderCopy(renderer, texture, NULL, &render);
}
void Sprite::renderScale(double scale)
{
    //SDL_Rect render { rect.x*scale, rect.y*scale, rect.w*scale, rect.h*scale };
    SDL_Rect render { rect.x/32, rect.y/32, rect.w/32, rect.h/32 };

    SDL_RenderCopy(renderer, texture, NULL, &render);
}

//prototypes
/**
// Returns a SDL_Rect with the coordinates passed,
// just like initializing an SDL_Rect, but can be
// passed directly into a function without a variable.
*/
SDL_Rect tempRect(int x, int y, int w, int h);
/**
// Handles collisions between two objects by nudging,
// 'first' outside of the bounds of 'second' in
// whatever direction is necessary.  Becuase of this,
// it is crucial that any immovable objects, such as
// walls and such must always be 'second' and ideally
// things in often motion, such as the player are 'first'.
*/
void handleCollision(Sprite* first, Sprite* second);
/**
// Returns 'TRUE' if 'first' and 'second' are
// overlapping, and 'FALSE' otherwise.
*/
bool colliding(Sprite* first, Sprite* second);
/**
// Returns 'TRUE' if 'first' and 'second' are
// beside each other with 0-pixels between
// them, and 'FALSE' otherwise.
*/
bool touching(Sprite* first, Sprite* second);
/**
// Stores information for all objects passed
// in 'objArr' in the file save_games/"num".save
*/
bool save(std::vector<Sprite*> objArr);
/**
// Reads information from save_games/"num".save
// and fills objects passed in 'objArr' with 
// this information to restore the saved state
*/
bool load(std::vector<Sprite*> objArr);
/**
// Reads information from 'firstFile' in layouts
// and spits out a Sprite with this information.
*/
Sprite* ltGen(char* firstFile);

int main ()
{
    //init of SDL
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
    SDL_Window* window = SDL_CreateWindow("Top_Down", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    //objs and init
    //create a pointer to every object needed
    //and initialize using init
    Sprite* player = new Sprite();
    player->init(renderer, SPRITE_TYPE_PLAYER);
    Sprite* bg = new Sprite();
    bg->init(renderer, SPRITE_TYPE_BG);
    Sprite* light = new Sprite();
    light->init(renderer, SPRITE_TYPE_LIGHT);
    Sprite* dim = new Sprite();
    dim->init(renderer, SPRITE_TYPE_DIM);
    Sprite* phone = new Sprite();
    phone->init(renderer, SPRITE_TYPE_PHONE);
    Sprite* border = new Sprite();
    border->init(renderer, SPRITE_TYPE_BORDER);
    Sprite* win = new Sprite();
    win->init(renderer, SPRITE_TYPE_NONE);

    std::vector<Sprite*> npcArr(NPC_COUNT);
    for (int i = 0; i < NPC_COUNT; ++i)
    {
        npcArr.at(i) = new Sprite();
        npcArr.at(i)->init(renderer, SPRITE_TYPE_NPC);
    }
    std::vector<Sprite*> termArr(TERM_COUNT);
    for (int i = 0; i < TERM_COUNT; ++i)
    {
        termArr.at(i) = new Sprite();
        termArr.at(i)->init(renderer, SPRITE_TYPE_TERM);
    }
    std::vector<Sprite*> wallArr(WALL_COUNT);
    for (int i = 0; i < WALL_COUNT; ++i)
    {
        wallArr.at(i) = new Sprite();
        if (i==0)
            wallArr.at(i)->init(renderer, SPRITE_TYPE_WALL, SPRITE_SUBTYPE_WALL_TOP);
        else if (i==1)
            wallArr.at(i)->init(renderer, SPRITE_TYPE_WALL, SPRITE_SUBTYPE_WALL_RIGHT);
        else if (i==2)
            wallArr.at(i)->init(renderer, SPRITE_TYPE_WALL, SPRITE_SUBTYPE_WALL_BOTTOM);
        else if (i==3)
            wallArr.at(i)->init(renderer, SPRITE_TYPE_WALL, SPRITE_SUBTYPE_WALL_LEFT);
        else
            break;
    }
    std::vector<Sprite*> budsArr(BUDS_COUNT);
    for (int i = 0; i < BUDS_COUNT; ++i)
    {
        budsArr.at(i) = new Sprite();
        budsArr.at(i)->init(renderer, SPRITE_TYPE_BUDS);
    }

    //audio objs
    Mix_Music* bgMus = Mix_LoadMUS("music/bgMus.wav");
    Mix_Chunk* walkfx = Mix_LoadWAV("music/walkfx.wav");
    Mix_Chunk* budsfx = Mix_LoadWAV("music/budsfx.wav");

    //other vars
    SDL_Rect cam{ ((MAP_WIDTH/2)-(WIN_WIDTH/2)+64), ((MAP_HEIGHT/2)-(WIN_HEIGHT/2)+64), WIN_WIDTH, WIN_HEIGHT };
    unsigned int framecount = 0;
    unsigned int walkframe = 0;
    InputMode inputMode = INPUT_MODE_PLAY;
    SDL_Rect win{ 0, 0, WIN_WIDTH, WIN_HEIGHT };
    SDL_Rect map{ (WIN_WIDTH*3/4), (WIN_HEIGHT*3/4), WIN_WIDTH/4, WIN_HEIGHT/4 };
    //for movement
    bool move[4] = { false, false, false, false };

    //set sound volumes and start bg music
    Mix_VolumeMusic( MIX_MAX_VOLUME / 32 );
    Mix_VolumeChunk( walkfx, MIX_MAX_VOLUME / 32 );
    Mix_VolumeChunk( budsfx, MIX_MAX_VOLUME / 16 );
    Mix_PlayMusic(bgMus, 0);

    //main loop
    while(true)
    {
        //event handling
        SDL_Event e;
        if (SDL_PollEvent(&e))
        {
            //keypress events
            if (e.type == SDL_KEYDOWN)
            {
                //ESC to bail
                if (e.key.keysym.sym == SDLK_ESCAPE)
                    break;

                //if playing game normally
                if (inputMode == INPUT_MODE_PLAY)
                {
                    //wasd for movement else stop
                    if (e.key.keysym.sym == SDLK_w)
                    {
                        Mix_PlayChannel(-1, walkfx, 1);
                        move[W] = true;
                        walkframe = framecount;
                    }
                    else if (e.key.keysym.sym == SDLK_a)
                    {
                        Mix_PlayChannel(-1, walkfx, 1);
                        move[A] = true;
                        walkframe = framecount;
                    }
                    else if (e.key.keysym.sym == SDLK_s)
                    {
                        Mix_PlayChannel(-1, walkfx, 1);
                        move[S] = true;
                        walkframe = framecount;
                    }
                    else if (e.key.keysym.sym == SDLK_d)
                    {
                        Mix_PlayChannel(-1, walkfx, 1);
                        move[D] = true;
                        walkframe = framecount;
                    }
                    //j to interact with earbuds
                    else if (e.key.keysym.sym == SDLK_j)
                    {
                        for (int i = 0; i < BUDS_COUNT; ++i)
                            if (touching(player, budsArr.at(i)))
                            {
                                Mix_PlayChannel(-1, budsfx, 0);
                                break;
                            }
                    }
                    //<tab> to toggle(open here) phone/menu
                    else if (e.key.keysym.sym == SDLK_TAB)
                    {
                        inputMode = INPUT_MODE_PHONE;
                        Mix_PauseMusic();
                    }
                }
                //if in phone/menu
                else if (inputMode == INPUT_MODE_PHONE)
                {
                    //s to save game
                    if (e.key.keysym.sym == SDLK_s)
                    {
                        std::vector<Sprite*> objArr;
                        objArr.emplace_back(player);
                        objArr.emplace_back(bg);
                        objArr.emplace_back(light);
                        for (int i = 0; i < NPC_COUNT; ++i)
                            objArr.emplace_back(npcArr.at(i));
                        for (int i = 0; i < WALL_COUNT; ++i)
                            objArr.emplace_back(wallArr.at(i));
                        for (int i = 0; i < TERM_COUNT; ++i)
                            objArr.emplace_back(termArr.at(i));

                        save(objArr);
                    }
                    //l to load previously saved game
                    else if (e.key.keysym.sym == SDLK_l)
                    {
                        std::vector<Sprite*> objArr;
                        objArr.emplace_back(player);
                        objArr.emplace_back(bg);
                        objArr.emplace_back(light);
                        for (int i = 0; i < NPC_COUNT; ++i)
                            objArr.emplace_back(npcArr.at(i));
                        for (int i = 0; i < WALL_COUNT; ++i)
                            objArr.emplace_back(wallArr.at(i));
                        for (int i = 0; i < TERM_COUNT; ++i)
                            objArr.emplace_back(termArr.at(i));

                        load(objArr);
                    }
                    //q to quit game from pause menu
                    else if (e.key.keysym.sym == SDLK_q)
                        break;
                    //<tab> to toggle(close here) phone/menu
                    else if (e.key.keysym.sym == SDLK_TAB)
                    {
                        inputMode = INPUT_MODE_PLAY;
                        Mix_ResumeMusic();
                    }
                }
            }

            //keyup events
            else if (e.type == SDL_KEYUP)
            {
                //wasd for movement else stop
                if (e.key.keysym.sym == SDLK_w)
                {
                    move[W] = false;
                }
                else if (e.key.keysym.sym == SDLK_a)
                {
                    move[A] = false;
                }
                else if (e.key.keysym.sym == SDLK_s)
                {
                    move[S] = false;
                }
                else if (e.key.keysym.sym == SDLK_d)
                {
                    move[D] = false;
                }
            }
        }

        //frame dependent actions
        //forces 15 frames of walking
/*
        if ((framecount - walkframe) % 15 == 14)
            for (int i = 0; i < 4; ++i)
                move[i] = false;
*/
        //resets framecount every minute to prevent overflow
        if ( framecount == 3600 )
            framecount = 0;
        framecount++;

        //movement
        if (move[W])
            player->setDir(player->getDir().x, -PLAYER_SPEED);
        if (move[A])
            player->setDir(-PLAYER_SPEED, player->getDir().y);
        if (move[S])
            player->setDir(player->getDir().x, PLAYER_SPEED);
        if (move[D])
            player->setDir(PLAYER_SPEED, player->getDir().y);
        //unmovement
        if (!move[W])
            if (player->getDir().y == -PLAYER_SPEED)
                player->setDir(player->getDir().x, 0);
        if (!move[A])
            if (player->getDir().x == -PLAYER_SPEED)
                player->setDir(0, player->getDir().y);
        if (!move[S])
            if (player->getDir().y == PLAYER_SPEED)
                player->setDir(player->getDir().x, 0);
        if (!move[D])
            if (player->getDir().x == PLAYER_SPEED)
                player->setDir(0, player->getDir().y);

        //update objs
        player->update();
        //update camera if necessary to follow player
        if ( cam.x + CAMBUF > player->getRect()->x )
            cam.x = player->getRect()->x - CAMBUF;
        if ( cam.x + cam.w - CAMBUF < player->getRect()->x + player->getRect()->w )
            cam.x = player->getRect()->x + player->getRect()->w - cam.w + CAMBUF;
        if ( cam.y + CAMBUF > player->getRect()->y )
            cam.y = player->getRect()->y - CAMBUF;
        if ( cam.y + cam.h - CAMBUF < player->getRect()->y + player->getRect()->h )
            cam.y = player->getRect()->y + player->getRect()->h - cam.h + CAMBUF;
        //collision handling
        for (int i = 0; i < WALL_COUNT; ++i)
            handleCollision(player, wallArr[i]);
        for (int i = 0; i < NPC_COUNT; ++i)
            handleCollision(player, npcArr[i]);
        for (int i = 0; i < TERM_COUNT; ++i)
            handleCollision(player, termArr[i]);
        for (int i = 0; i < BUDS_COUNT; ++i)
            handleCollision(player, budsArr[i]);
        //follow player with light
        //light after collisions to make sure it doesn't "lead" where pushing
        light->setPos(player->getRect()->x - (light->getRect()->w / 2), player->getRect()->y - (light->getRect()->h / 2));

        //rendering
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        bg->render(cam);
        for (int i = 0; i < WALL_COUNT; ++i)
            wallArr.at(i)->render(cam);
        for (int i = 0; i < TERM_COUNT; ++i)
            termArr.at(i)->render(cam);
        for (int i = 0; i < NPC_COUNT; ++i)
            npcArr.at(i)->render(cam);
        for (int i = 0; i < BUDS_COUNT; ++i)
            budsArr.at(i)->render(cam);
        light->render(cam);
        player->render(cam);

        //minimap
        SDL_RenderSetViewport(renderer, &map);
        bg->renderScale(SCALE);
        for (int i = 0; i < WALL_COUNT; ++i)
            wallArr.at(i)->renderScale(SCALE);
        for (int i = 0; i < TERM_COUNT; ++i)
            termArr.at(i)->renderScale(SCALE);
        for (int i = 0; i < NPC_COUNT; ++i)
            npcArr.at(i)->renderScale(SCALE);
        for (int i = 0; i < BUDS_COUNT; ++i)
            budsArr.at(i)->renderScale(SCALE);
        light->renderScale(SCALE);
        player->renderScale(SCALE);
        border->renderScale(SCALE);
        SDL_RenderSetViewport(renderer, &win);
        
        //phone only if applicable
        if (inputMode == INPUT_MODE_PHONE)
        {
            dim->render();
            phone->render();
        }

        SDL_RenderPresent(renderer);
    }

    //clean up
    //reverse of what I did to
    //init all these before the loop
    delete player;
    player = nullptr;
    delete bg;
    bg = nullptr;
    delete light;
    light = nullptr;
    delete dim;
    dim = nullptr;
    delete phone;
    phone = nullptr;
    delete border;
    border = nullptr;
    for (int i = 0; i < NPC_COUNT; ++i)
    {
        delete npcArr.at(i);
        npcArr.at(i) = nullptr;
    }
    for (int i = 0; i < TERM_COUNT; ++i)
    {
        delete termArr.at(i);
        termArr.at(i) = nullptr;
    }
    for (int i = 0; i < WALL_COUNT; ++i)
    {
        delete wallArr.at(i);
        wallArr.at(i) = nullptr;
    }
    for (int i = 0; i < BUDS_COUNT; ++i)
    {
        delete budsArr.at(i);
        budsArr.at(i) = nullptr;
    }
    
    Mix_Quit();
    SDL_Quit();
    return 0;
}

SDL_Rect tempRect(int x, int y, int w, int h)
{
    SDL_Rect rect{x, y, w, h};
    return rect;
}
void handleCollision(Sprite* first, Sprite* second)
{
    //IMPORTANT NOTE!!!!!
    //'first' object will be the object buffeted out of
    //the way.  Therefore, 'first' must be an object that moves,
    //ideally the player or some other regularly moving object.

    //bail if not even touching
    if (!colliding(first, second))
        return;

    //second above first
    if (first->getRect()->y > second->getRect()->y + second->getRect()->h - COLLISION_OVERLAP)
        first->setPos(first->getRect()->x, second->getRect()->y + second->getRect()->h);

    //second to the right of first
    if (first->getRect()->x + first->getRect()->w < second->getRect()->x + COLLISION_OVERLAP)
        first->setPos(second->getRect()->x - first->getRect()->w, first->getRect()->y);

    //second below first
    if (first->getRect()->y + first->getRect()->h < second->getRect()->y + COLLISION_OVERLAP)
        first->setPos(first->getRect()->x, second->getRect()->y - first->getRect()->h);

    //second to the left of first
    if (first->getRect()->x > second->getRect()->x + second->getRect()->w - COLLISION_OVERLAP)
        first->setPos(second->getRect()->x + second->getRect()->w, first->getRect()->y);
}
bool colliding(Sprite* first, Sprite* second)
{
    if (first->getRect()->x < second->getRect()->x + second->getRect()->w &&
        second->getRect()->x < first->getRect()->x + first->getRect()->w &&
        first->getRect()->y < second->getRect()->y + second->getRect()->h &&
        second->getRect()->y < first->getRect()->y + first->getRect()->h) 
        return true;

    return false;
}
bool touching(Sprite* first, Sprite* second)
{
    if (first->getRect()->x - 1 < second->getRect()->x + second->getRect()->w &&
        second->getRect()->x < first->getRect()->x + first->getRect()->w + 1 &&
        first->getRect()->y - 1 < second->getRect()->y + second->getRect()->h &&
        second->getRect()->y < first->getRect()->y + first->getRect()->h + 1) 
        return true;

    return false;
}
bool save(std::vector<Sprite*> objArr)
{
    bool pass = true;
    std::string filename = "save_games/1.save";
    std::ofstream saveFile(filename);

    if(!saveFile.is_open())
        pass = false;

    for (int i = 0; i < int(objArr.size()); ++i)
    {
        saveFile << objArr.at(i)->getType() << " ";
        saveFile << objArr.at(i)->getSubtype() << " ";
        saveFile << objArr.at(i)->getRect()->x << " ";
        saveFile << objArr.at(i)->getRect()->y << "\n";
    }
    saveFile.close();

    return pass;
}
bool load(std::vector<Sprite*> objArr)
{
    bool pass = true;
    std::string filename = "save_games/1.save";
    std::ifstream saveFile(filename);

    if(!saveFile.is_open())
        pass = false;

    bool* matched = new bool[objArr.size()];
    for (int i = 0; i < int(objArr.size()); ++i)
        matched[i] = false;

    int temp = 0;
    Type curType = SPRITE_TYPE_NONE;
    Subtype curSubtype = SPRITE_SUBTYPE_NONE;
    Vec curPos;
    for (int i = 0; i < int(objArr.size()); ++i)
    {
        saveFile >> temp;
        curType = Type(temp);
        saveFile >> temp;
        curSubtype = Subtype(temp);
        saveFile >> curPos.x;
        saveFile >> curPos.y;
        for (int j = 0; j < int(objArr.size()); ++j)
        {
            if (matched[j] == true)
                continue;

            if (objArr.at(j)->getType() == curType)
                if (objArr.at(j)->getSubtype() == curSubtype)
                {
                    objArr.at(j)->setPos(curPos.x, curPos.y);
                    matched[j] = true;
                }
        }
    }
    saveFile.close();
    delete[] matched;

    return pass;
}
Sprite* ltGen(char* firstFile)
{
    //varaibles
    std::ifstream curLt(firstFile);
    std::string curline,
        desc,
        filename,
        rectx, recty, rectw, recth,
        adjList[10],
        //up[10], right[10], down[10], left[10],
        next;

    //confirm file opening successful
    if ( !curLt.is_open() )
    {
        std::cout << "Error: file not able to be opened, please check\nexistence, permissions, and filename and try again." << std::endl;
        return nullptr;
    }

    //read file and collect information
    for (; std::getline(curLt >> std::ws, curline);)
    {
        if ( curline == "filename:" )
        {
            std::getline(curLt >> std::ws, filename);
        }
        else if ( curline == "rect:" )
        {
            std::getline(curLt >> std::ws, rectx);
            std::getline(curLt >> std::ws, recty);
            std::getline(curLt >> std::ws, rectw);
            std::getline(curLt >> std::ws, recth);
        }
        else if ( curline == "adjacent" )
        {
            //for now check all adjacent blocks for collision, still not bad
            for (int i = 0; curline != ""; std::getline(curLt >> std::ws, adjList[i++]) )
                ;
                /**
                if ( curline == "up:" )
                    for (int i = 0; curline != "right" || curline != "down" || curline != "left"; std::getline(curLt >> std::ws, curline), ++i)
                        std::getline(curLt >> std::ws, up[i]);
                else if ( curline == "right:" )
                    for (int i = 0; curline != "up" || curline != "down" || curline != "left"; std::getline(curLt >> std::ws, curline), ++i)
                        std::getline(curLt >> std::ws, right[i]);
                else if ( curline == "down:" )
                    for (int i = 0; curline != "right" || curline != "up" || curline != "left"; std::getline(curLt >> std::ws, curline), ++i)
                        std::getline(curLt >> std::ws, down[i]);
                else if ( curline == "left:" )
                    for (int i = 0; curline != "right" || curline != "down" || curline != "up"; std::getline(curLt >> std::ws, curline), ++i)
                        std::getline(curLt >> std::ws, left[i]);
                */
        }
        //determine what next file is
        else if ( curline == "next:" )
        {
            std::getline(curLt >> std::ws, next);
            curLt.close();
        }
    }

    //pass all this information on to generate a Sprite* with these specs
    //SDL_Rect tempRect{atoi(rectx.c_str()), atoi(recty.c_str()), atoi(rectw.c_str()), atoi(recth.c_str())};
    Sprite* retSprite = new Sprite();//(filename, tempRect, adjList);

    return retSprite;
}
